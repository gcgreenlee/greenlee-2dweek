﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Threading;


public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;

    public SpriteRenderer spriteRenderer;
    public Animator animator;

    float currentZRotation = 0;
    public TextMeshProUGUI counter;
    public int coinCounter;
    public CoinCollection coinCollection;
    public GameObject [] hiddenCoins;

    
    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        coinCounter = 0;
        foreach(GameObject coin in hiddenCoins)
        {
            coin.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (coinCounter >= 7)
        {
            counter.text = "You Win!";
            Thread.Sleep(2000);
            Application.Quit();
        }
        if( Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
               
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            foreach(GameObject coin in hiddenCoins)
            {
                coin.SetActive(true);
            }
            counter.text = "Coins Collected: ";
        }
        else if (other.gameObject.CompareTag("HiddenCoin"))
        {
            other.gameObject.SetActive(false);
            coinCounter += 1;
            counter.text = "Coins Collected: " + coinCounter.ToString();
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Vertical");

        float verticalInput = Input.GetAxis("Horizontal");


        /*
        Quaternion rot = transform.rotation;

        currentZRotation += horizontalInput * Time.deltaTime;

        rot.SetEulerRotation(new Vector3(0, 0, currentZRotation));

        transform.rotation = rot;
        */


        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);
        rB2D.velocity = new Vector2(verticalInput * runSpeed * Time.deltaTime, rB2D.velocity.x);



        //rB2D.AddForce(new Vector2(0, 1000));

        //rB2D.AddForce(new Vector2(horizontalInput, 0));

        if( rB2D.velocity.x > 0)
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }


        if ( Mathf.Abs(horizontalInput) > 0f || Mathf.Abs(verticalInput) > 0f)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
        
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }
}
